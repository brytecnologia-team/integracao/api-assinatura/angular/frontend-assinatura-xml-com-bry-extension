import { Component, OnInit, Input } from '@angular/core';
import { Router } from "@angular/router";
import {FormGroup, FormBuilder, FormControl, Validators, AbstractControl} from '@angular/forms'
import {SignatureService} from './assinatura.service'
import {tap} from 'rxjs/operators'
declare var BryApiModule: any;



@Component({
  selector: 'cms-assinatura',
  templateUrl: './assinatura.component.html',
  styleUrls: ['./assinatura.component.css']
})
export class InitializeComponent implements OnInit {


  signatureForm: FormGroup;

  inicializationResult;

  finalizationResult;

  file: any;

  signedAttributes;

  initializedDocument;

  base64Certificate;

  title =  'XML Signature';

  auxSignatureValue;

  signature;

  token;

  nonce;

  documentNonce;

  constructor(private signatureService: SignatureService,
              private router: Router,
              private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    this.signatureForm = this.formBuilder.group({
      token:  new FormControl('', { validators: [Validators.required], updateOn: 'blur'}),
      binaryContent: new FormControl('false', { validators: [Validators.required], updateOn: 'blur'}),
      profile: new FormControl('BASIC', { validators: [Validators.required], updateOn: 'blur'}),
      hashAlgorithm: new FormControl('SHA256', { validators: [Validators.required], updateOn: 'blur'}),
      certificate: new FormControl(''),
      operationType: new FormControl('SIGNATURE', { validators: [Validators.required], updateOn: 'blur'}),
      signatureFormat: new FormControl('ENVELOPED', { validators: [Validators.required], updateOn: 'blur'}),
      file: this.formBuilder.control(null, [Validators.required]),
      signature: new FormControl('')
    })

  }

  initialize(signatureForm: FormGroup, base64Certificate) {

    this.token = this.signatureForm.get('token').value;

    this.base64Certificate = base64Certificate;

    this.nonce = this.signatureService.generateRandonNumber();
    console.log('Nonce', this.nonce);

    this.documentNonce = this.signatureService.generateRandonNumber();
    console.log('Document nonce', this.documentNonce);

    this.signatureService.initialize(this.nonce, this.documentNonce, signatureForm, this.base64Certificate, this.file, this.token)
      .pipe(
        tap((result: any) => {
          this.inicializationResult = result;
        }
        )
      ).subscribe( async (result: any) => {

        console.log(`Inicialization result sem o parse: ${JSON.stringify(result)}`);

        this.signedAttributes = result.signedAttributes[0].content ;

        this.initializedDocument = result.initializedDocuments[0].content

        console.log(`Inicialization result: ${this.signedAttributes}`);

        const preparedExtensionData = await this.prepareInputDataForExtension(result);

        console.log('Dados preparados da extensao:', preparedExtensionData);

        const signedData = await this.signDataByExtension(preparedExtensionData);

        console.log('Dados assinados pela extensao: ', signedData);

        var objPreparedExtensionData = JSON.parse(signedData);

        this.auxSignatureValue = objPreparedExtensionData.assinaturas[0].hashes[0];

        console.log('Encrypted signed attributes and Base64 encoded: ', this.auxSignatureValue);

        this.signatureService.finalize(this.nonce, this.documentNonce, signatureForm, this.base64Certificate, this.file, this.initializedDocument, this.auxSignatureValue, this.token)
            .pipe(
                 tap((result: any) => {
                  this.finalizationResult = result;
                  }
            )
        ).subscribe((result: any) => {

          //console.log(`Finalization result: ${result.signatures[0].content}`);

          this.signature = JSON.stringify(result)

          this.signatureForm.get('signature').setValue(this.signature);

          alert('Signature realized successful!');

        })

      } )

  }

  clear(){
    this.signature = null;
    this.file = null;
    this.signatureForm.get('file').reset()
    this.signatureForm.get('file').setValue(null);
    this.signatureForm.get('hashAlgorithm').setValue('SHA256');
  }


  fillCertificateDataForm() {
    BryApiModule.fillCertificateDataForm();
  }

  listCertificates() {
    BryApiModule.listCertificates();
  }

  prepareInputDataForExtension(signedAttributes): any{
    const response = BryApiModule.prepareDataForExtension(signedAttributes);
    return response;
  }

  async signDataByExtension(prepareInputData) {
    const signedData = BryApiModule.sign(prepareInputData);
    return signedData;
  }


  upload(event){
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];

    }
  }


}
