const PROXY_CONFIG = [{
    context: [
        '/api'
    ],
    target: 'http://localhost:3333/',
    secure: false,
    logLevel: 'debug',
    changeOrigin: true,
    pathRewrite: {
        '^/api': '' // rewrite path
    }
}]

module.exports = PROXY_CONFIG;